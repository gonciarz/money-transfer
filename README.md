# Money transfer service

## Environment requirements

- JDK 12+

## How to build/run

    ./gradlew clean build
    ./gradlew run
    
By default application listens on a port 8080.

## API documentation

    http://localhost:8080/swagger

## Assumptions

1. We have two parties for each transaction: 1 sender and 1 receiver (beneficiary)
2. There is only one account per party (account holder)
3. There is only one currency for all transactions (that property was skipped)
4. All accounts/money exist only in our service, we do not care about external ones
5. No fees are charged
6. Accounts cannot be disabled or frozen
7. Account's balance can be only positive 
7. MVP, no authentication or reach features
8. Service must handle several clients
9. Minimalist audit log was introduced

## Implementation

1. Source/target compatibility with Java 11
2. No heavy frameworks: Micronaut (standalone service based on netty), Vavr, Lombok, MapStruct
3. No DB was used. Data is stored in memory.
4. Money is stored in shifted (multiplied by 100) *long* data type by design (e.g. 100.75 USD is stored as 100_75L)

    
    1.2 * 10^15 - all non/physical money
    9.2 * 10^18 - long max range
    9.2 * 10^16 - shifted long (with cents)    
     
5. Account's ID (UUID) is an Account's number.
6. REST API exposed by Swagger

## Known issues:

Service cannot be run with JDK 11 (u10 and lower): 
https://bugs.openjdk.java.net/browse/JDK-8210483
