package rg.rev.utils;

import javax.inject.Singleton;
import java.time.LocalDateTime;

@Singleton
public class TimeProvider {

    public LocalDateTime now() {
        return LocalDateTime.now();
    }

}
