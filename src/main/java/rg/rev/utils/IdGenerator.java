package rg.rev.utils;

import javax.inject.Singleton;
import java.util.UUID;

@Singleton
public class IdGenerator {

    public UUID randomUUID() {
        return UUID.randomUUID();
    }

}
