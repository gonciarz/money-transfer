package rg.rev.model;

public enum TransferState {

    NEW,
    ACCEPTED,
    COMPLETED,
    ERROR
}
