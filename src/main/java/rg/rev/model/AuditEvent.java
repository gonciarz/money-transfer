package rg.rev.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import rg.rev.errors.ErrorCause;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuditEvent {

    UUID transactionId;
    LocalDateTime timestamp;
    UUID senderNumber;
    UUID receiverNumber;
    long transferAmount;
    TransferState transactionState;
    ErrorCause errorCause;

}
