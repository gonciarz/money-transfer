package rg.rev.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import rg.rev.errors.ErrorCause;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Transaction {

    UUID id;
    LocalDateTime timestamp;

    UUID senderNumber;
    UUID receiverNumber;
    long amount;

    TransferState status;
    ErrorCause errorCause;
}
