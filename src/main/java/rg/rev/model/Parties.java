package rg.rev.model;

import lombok.Value;

@Value
public class Parties {
    Account sender;
    Account receiver;
}