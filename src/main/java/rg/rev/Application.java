package rg.rev;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

@OpenAPIDefinition(
        info = @Info(
                title = "Money Transfer",
                version = "1.0",
                description = "Service for transferring money between accounts",
                license = @License(name = "GPL v3", url = "https://www.gnu.org/licenses/gpl-3.0.txt"),
                contact = @Contact(url = "https://linkedin.com/in/gonciarz", name = "Robert Gonciarz")
        )
)
public class Application {

    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}