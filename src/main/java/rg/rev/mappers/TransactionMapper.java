package rg.rev.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import rg.rev.dto.TransferDto;
import rg.rev.model.AuditEvent;
import rg.rev.model.Transaction;

@Mapper(componentModel = "jsr330")
public interface TransactionMapper {
    
    @Mapping(source = "transaction.id", target = "transactionId")
    @Mapping(source = "transaction.timestamp", target = "timestamp")
    @Mapping(source = "transaction.senderNumber", target = "senderNumber")
    @Mapping(source = "transaction.receiverNumber", target = "receiverNumber")
    @Mapping(source = "transaction.amount", target = "transferAmount")
    @Mapping(source = "transaction.status", target = "transactionState")
    @Mapping(source = "transaction.errorCause", target = "errorCause")
    AuditEvent toAuditEvent(Transaction transaction);

    @Mapping(source = "id", target = "transactionId")
    @Mapping(source = "timestamp", target = "timestamp")
    @Mapping(source = "senderNumber", target = "senderNumber")
    @Mapping(source = "receiverNumber", target = "receiverNumber")
    @Mapping(source = "amount", target = "amount")
    TransferDto toTransferDto(Transaction transaction);

}