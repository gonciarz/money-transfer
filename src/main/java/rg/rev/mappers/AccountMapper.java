package rg.rev.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import rg.rev.dto.AccountDto;
import rg.rev.model.Account;

@Mapper(componentModel = "jsr330")
public interface AccountMapper {

    @Mapping(source = "id", target = "number")
    @Mapping(source = "balance", target = "balance")
    @Mapping(source = "holderName", target = "holderName")
    AccountDto toAccountDto(Account account);
}