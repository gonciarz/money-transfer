package rg.rev.errors;

import io.micronaut.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static io.micronaut.http.HttpStatus.*;

@Getter
@AllArgsConstructor
public enum ErrorCause {

    SAME_ACCOUNTS("E1001", BAD_REQUEST, "Sender's and Receiver's accounts are the same"),
    SENDER_NOT_EXIST("E1002", NOT_FOUND, "Sender's account does not exist"),
    RECEIVER_NOT_EXIST("E1003", NOT_FOUND, "Receiver's account does not exist"),
    SENDER_INSUFFICIENT_BALANCE("E2001", BAD_REQUEST, "Insufficient sender's balance"),
    INTERNAL_ERROR("E5001", INTERNAL_SERVER_ERROR, "Internal error"),
    SUCCESS("E0", OK, "OK");

    private String errorCode;
    private HttpStatus httpStatus;
    private final String message;

}
