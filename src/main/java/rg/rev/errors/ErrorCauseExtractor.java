package rg.rev.errors;

import io.vavr.control.Try;

import javax.annotation.Nonnull;
import javax.inject.Singleton;

@Singleton
public class ErrorCauseExtractor {

    public Try<ErrorCause> extract(@Nonnull Throwable throwable) {
        return Try.success(throwable)
                .filter(AppException.class::isInstance)
                .map(AppException.class::cast)
                .map(AppException::getErrorCause);
    }

}
