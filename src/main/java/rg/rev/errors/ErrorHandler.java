package rg.rev.errors;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MutableHttpResponse;
import rg.rev.dto.ErrorResponse;

import javax.annotation.Nonnull;
import javax.inject.Singleton;
import java.util.Optional;

import static rg.rev.errors.ErrorCause.INTERNAL_ERROR;

@Singleton
public class ErrorHandler {

    private final ErrorCauseExtractor errorCauseExtractor;

    public ErrorHandler(ErrorCauseExtractor errorCauseExtractor) {
        this.errorCauseExtractor = errorCauseExtractor;
    }

    public MutableHttpResponse createErrorResponse(@Nonnull Throwable throwable) {
        return errorCauseExtractor.extract(throwable)
                .map(this::toHttpResponse)
                .getOrElse(() -> toHttpResponse(throwable));
    }

    private MutableHttpResponse<ErrorResponse> toHttpResponse(@Nonnull ErrorCause errorCause) {
        return HttpResponse
                .<ErrorResponse>status(errorCause.getHttpStatus())
                .body(new ErrorResponse(errorCause.getErrorCode(), errorCause.getMessage()));
    }

    private MutableHttpResponse<ErrorResponse> toHttpResponse(@Nonnull Throwable throwable) {
        return HttpResponse.serverError(
                new ErrorResponse(INTERNAL_ERROR.getErrorCode(),
                        Optional.ofNullable(throwable.getMessage())
                                .orElse(INTERNAL_ERROR.getMessage())
                )
        );
    }

}
