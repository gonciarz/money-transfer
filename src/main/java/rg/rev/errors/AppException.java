package rg.rev.errors;

import lombok.Getter;

import javax.annotation.Nonnull;

@Getter
public class AppException extends Exception {

    private final ErrorCause errorCause;

    public AppException(ErrorCause errorCause) {
        super(errorCause.getMessage());
        this.errorCause = errorCause;
    }

    public static AppException error(@Nonnull ErrorCause errorCause) {
        return new AppException(errorCause);
    }
}
