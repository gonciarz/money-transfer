package rg.rev.controllers;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import rg.rev.model.AuditEvent;
import rg.rev.services.AuditService;

import javax.inject.Inject;
import java.util.Collection;

@Controller("/audits")
@ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "OK"),
        @ApiResponse(responseCode = "400", description = "Bad request"),
        @ApiResponse(responseCode = "500", description = "Internal server error")
})
@Tag(name = "Audit log")
public class AuditController {

    private final AuditService auditService;

    @Inject
    public AuditController(AuditService auditService) {
        this.auditService = auditService;
    }

    @Get(produces = MediaType.APPLICATION_JSON)
    public Collection<AuditEvent> list() {
        return auditService.allAuditEvents();
    }
}
