package rg.rev.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.validation.Validated;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import rg.rev.dto.TransferDto;
import rg.rev.errors.ErrorHandler;
import rg.rev.mappers.TransactionMapper;
import rg.rev.services.AccountService;

import javax.inject.Inject;
import javax.validation.Valid;

@Validated
@Controller("/transfers")
@ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "OK"),
        @ApiResponse(responseCode = "400", description = "Bad request"),
        @ApiResponse(responseCode = "500", description = "Internal server error")
})
@Tag(name = "Transfer money")
public class TransferController {

    private final AccountService accountService;
    private final TransactionMapper transactionMapper;
    private final ErrorHandler errorHandler;

    @Inject
    public TransferController(AccountService accountService,
                              TransactionMapper transactionMapper,
                              ErrorHandler errorHandler) {
        this.accountService = accountService;
        this.transactionMapper = transactionMapper;
        this.errorHandler = errorHandler;
    }

    @ApiResponse(responseCode = "404", description = "Account not found")
    @Post(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public HttpResponse<TransferDto> transfer(@Body @Valid TransferDto transferRequest) {
        return accountService.transfer(transferRequest)
                .map(transactionMapper::toTransferDto)
                .map(HttpResponse::ok)
                .getOrElseGet(errorHandler::createErrorResponse);
    }

}
