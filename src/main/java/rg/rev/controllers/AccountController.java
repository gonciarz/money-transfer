package rg.rev.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import rg.rev.dto.AccountDto;
import rg.rev.errors.ErrorHandler;
import rg.rev.mappers.AccountMapper;
import rg.rev.services.AccountService;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Controller("/accounts")
@ApiResponses(value = {
        @ApiResponse(responseCode = "400", description = "Bad request"),
        @ApiResponse(responseCode = "500", description = "Internal server error"),
})
@Tag(name = "Account manager")
public class AccountController {

    private final AccountService accountService;
    private final AccountMapper accountMapper;
    private final ErrorHandler errorHandler;

    @Inject
    public AccountController(AccountService accountService,
                             AccountMapper accountMapper,
                             ErrorHandler errorHandler) {
        this.accountService = accountService;
        this.accountMapper = accountMapper;
        this.errorHandler = errorHandler;
    }

    @ApiResponse(responseCode = "201", description = "Created")
    @Post(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public HttpResponse<AccountDto> create(@Body AccountDto accountDto) {
        return accountService.createAccount(accountDto.getHolderName(), accountDto.getBalance())
                .map(accountMapper::toAccountDto)
                .map(HttpResponse::created)
                .getOrElseGet(errorHandler::createErrorResponse);
    }

    @ApiResponse(responseCode = "200", description = "OK")
    @Get(produces = MediaType.APPLICATION_JSON)
    public List<AccountDto> list() {
        return accountService.allAccounts().stream()
                .map(accountMapper::toAccountDto)
                .collect(Collectors.toList());
    }

}
