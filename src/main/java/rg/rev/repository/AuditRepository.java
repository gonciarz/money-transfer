package rg.rev.repository;

import io.vavr.control.Try;
import rg.rev.model.AuditEvent;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.Queue;
import java.util.UUID;

@Singleton
public class AuditRepository implements SimpleRepository<AuditEvent, UUID> {

    private final Queue<AuditEvent> auditEvents;

    @Inject
    public AuditRepository(@Named("auditEvents") Queue<AuditEvent> auditEvents) {
        this.auditEvents = auditEvents;
    }

    @Override
    public Try<AuditEvent> save(@Nonnull AuditEvent auditEvent) {
        return Try.of(() -> auditEvents.add(auditEvent))
                .filter(result -> result)
                .map(result -> auditEvent);
    }

    @Override
    public Collection<AuditEvent> findAll() {
        return auditEvents;
    }

    @Override
    public void deleteAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Try<AuditEvent> findById(@Nonnull UUID uuid) {
        throw new UnsupportedOperationException();
    }

}
