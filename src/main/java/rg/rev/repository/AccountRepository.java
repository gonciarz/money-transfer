package rg.rev.repository;

import io.vavr.control.Try;
import rg.rev.model.Account;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

@Singleton
public class AccountRepository implements SimpleRepository<Account, UUID> {

    private final Map<UUID, Account> accounts;

    @Inject
    public AccountRepository(@Named("accounts") Map<UUID, Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public Try<Account> save(@Nonnull Account account) {
        return Try.of(() -> {
            accounts.put(account.getId(), account);
            return account;
        });
    }
    
    @Override
    public Try<Account> findById(@Nonnull UUID uuid) {
        return Try.of(() -> accounts.get(uuid));
    }

    @Override
    public Collection<Account> findAll() {
        return accounts.values();
    }

    @Override
    public void deleteAll() {
        accounts.clear();
    }

}
