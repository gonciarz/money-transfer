package rg.rev.repository;

import io.vavr.control.Try;

import javax.annotation.Nonnull;
import java.util.Collection;

public interface SimpleRepository<T, ID> {

    Try<T> save(@Nonnull T entity);

    Try<T> findById(@Nonnull ID id);

    Collection<T> findAll();

    void deleteAll(); // for testing purpose
}
