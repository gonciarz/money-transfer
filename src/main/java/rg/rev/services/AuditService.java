package rg.rev.services;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import rg.rev.model.AuditEvent;
import rg.rev.repository.AuditRepository;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collection;

@Singleton
@Slf4j
public class AuditService {

    private final AuditRepository auditRepository;

    @Inject
    public AuditService(AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    public Try<AuditEvent> saveAuditEvent(@Nonnull AuditEvent auditEvent) {
        log.info("Saving auditEvent: {}", auditEvent);
        return auditRepository.save(auditEvent);
    }

    public Collection<AuditEvent> allAuditEvents() {
        return auditRepository.findAll();
    }

}
