package rg.rev.services;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import rg.rev.dto.TransferDto;
import rg.rev.errors.ErrorCause;
import rg.rev.errors.ErrorCauseExtractor;
import rg.rev.mappers.TransactionMapper;
import rg.rev.model.Account;
import rg.rev.model.Parties;
import rg.rev.model.Transaction;
import rg.rev.model.Transaction.TransactionBuilder;
import rg.rev.repository.AccountRepository;
import rg.rev.utils.IdGenerator;
import rg.rev.utils.TimeProvider;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static rg.rev.errors.AppException.error;
import static rg.rev.errors.ErrorCause.INTERNAL_ERROR;
import static rg.rev.errors.ErrorCause.RECEIVER_NOT_EXIST;
import static rg.rev.errors.ErrorCause.SAME_ACCOUNTS;
import static rg.rev.errors.ErrorCause.SENDER_INSUFFICIENT_BALANCE;
import static rg.rev.errors.ErrorCause.SENDER_NOT_EXIST;
import static rg.rev.errors.ErrorCause.SUCCESS;
import static rg.rev.model.TransferState.ACCEPTED;
import static rg.rev.model.TransferState.COMPLETED;
import static rg.rev.model.TransferState.ERROR;
import static rg.rev.model.TransferState.NEW;

@Singleton
@Slf4j
public class AccountService {

    private static final int TIMEOUT_LOCK_MILLIS = 1000;

    private final AccountRepository accountRepository;
    private final AuditService auditService;
    private final TransactionMapper transactionMapper;
    private final ErrorCauseExtractor errorCauseExtractor;
    private final IdGenerator idGenerator;
    private final TimeProvider timeProvider;
    private final Map<UUID, ReentrantReadWriteLock> accountLocks;

    @Inject
    public AccountService(AccountRepository accountRepository,
                          AuditService auditService,
                          TransactionMapper transactionMapper,
                          ErrorCauseExtractor errorCauseExtractor,
                          IdGenerator idGenerator, TimeProvider timeProvider,
                          @Named("accountLocks") Map<UUID, ReentrantReadWriteLock> accountLocks) {
        this.accountRepository = accountRepository;
        this.auditService = auditService;
        this.transactionMapper = transactionMapper;
        this.errorCauseExtractor = errorCauseExtractor;
        this.idGenerator = idGenerator;
        this.timeProvider = timeProvider;
        this.accountLocks = accountLocks;
    }

    public Try<Account> createAccount(@Nonnull String name, long balance) {
        var id = idGenerator.randomUUID();
        var account = new Account(id, balance, name);
        accountLocks.putIfAbsent(id, new ReentrantReadWriteLock(true));
        return accountRepository.save(account)
                .onSuccess(createAccount -> log.info("Account created: {}", createAccount))
                .onFailure(th -> log.error("Cannot create account for " + name, th));
    }

    public Collection<Account> allAccounts() {
        return accountRepository.findAll();
    }

    public Try<Transaction> transfer(@Nonnull TransferDto transferRequest) {
        var transactionBuilder = createTransactionBuilder(transferRequest);

        return Try.success(transferRequest)
                .filter(this::validateAccountsDiffers, () -> error(SAME_ACCOUNTS))
                .flatMap(this::findParties)
                .andThen(() -> transactionBuilder.status(ACCEPTED))
                .flatMap(parties -> updatePartiesBalance(parties, transferRequest.getAmount()))
                .map(any -> transactionBuilder.status(COMPLETED))
                .map(TransactionBuilder::build)
                .onFailure(th -> failTransaction(transactionBuilder, th))
                .andFinally(() -> audit(transactionBuilder.build()));
    }

    private void failTransaction(@Nonnull TransactionBuilder transactionBuilder, @Nonnull Throwable throwable) {
        var errorCause = errorCauseExtractor.extract(throwable).getOrElse(INTERNAL_ERROR);
        transactionBuilder.status(ERROR).errorCause(errorCause);
        log.error("Failed transaction: {}", transactionBuilder.build());
    }

    private Try<Parties> updatePartiesBalance(@Nonnull Parties parties, long deltaAmount) {
        var updatedSender = updateAccountBalance(parties.getSender(), -deltaAmount, true);
        var updatedReceiver = updateAccountBalance(parties.getReceiver(), deltaAmount, false);
        return updatedSender.flatMap(s -> updatedReceiver.map(r -> new Parties(s, r)));
    }

    /**
     * This is the core of synchronization mechanism in this service.
     * Each account is combined with corresponding lock instance.
     * ReentrantReadWriteLock was used with fairness policy to keep an order of incoming requests.
     * Independent treads can enter this method without any conflict as long as they handle different accounts.
     * If an account is used as a receiver (beneficiary) even if it's balance will be modified, we shouldn't block it.
     * Thus read lock was used for it even though it's balance will changed (increased).
     * On the other hand a sender account always need to be blocked and write lock was used to achieve that.
     *
     * @param account     - Account
     * @param deltaAmount - amount of money to transfer
     * @param isSender    - is it sender or receiver
     * @return - Try of updated account
     */
    private Try<Account> updateAccountBalance(@Nonnull Account account, long deltaAmount, boolean isSender) {
        var rootLock = accountLocks.get(account.getId());
        var lock = isSender ? rootLock.writeLock() : rootLock.readLock();
        try {
            return Try.of(() -> lock.tryLock(TIMEOUT_LOCK_MILLIS, MILLISECONDS))
                    .filter(any -> !isSender || validateAccountBalance(account, deltaAmount),
                            () -> error(SENDER_INSUFFICIENT_BALANCE))
                    .map(result -> updateAccountBalance(account, deltaAmount));
//                    .flatMap(accountRepository::save); // for micronaut-data
        } finally {
            lock.unlock();
        }
    }

    private boolean validateAccountBalance(@Nonnull Account account, long deltaAmount) {
        return account.getBalance() + deltaAmount >= 0;
    }

    private Account updateAccountBalance(@Nonnull Account account, long deltaAmount) {
        account.setBalance(account.getBalance() + deltaAmount);
        return account;
    }

    private Try<Parties> findParties(@Nonnull TransferDto transferRequest) {
        return findPartiesByNumber(transferRequest.getSenderNumber(), transferRequest.getReceiverNumber());
    }

    private Try<Parties> findPartiesByNumber(@Nonnull UUID senderNumber, @Nonnull UUID receiverNumber) {
        var sender = findByNumber(senderNumber, SENDER_NOT_EXIST);
        var receiver = findByNumber(receiverNumber, RECEIVER_NOT_EXIST);
        return sender.flatMap(s -> receiver.map(r -> new Parties(s, r)));
    }

    private Try<Account> findByNumber(@Nonnull UUID number, @Nonnull ErrorCause errorOnFailure) {
        return accountRepository.findById(number)
                .filter(Objects::nonNull, () -> error(errorOnFailure));
    }

    private boolean validateAccountsDiffers(@Nonnull TransferDto request) {
        return !request.getSenderNumber().equals(request.getReceiverNumber());
    }

    private TransactionBuilder createTransactionBuilder(@Nonnull TransferDto request) {
        return Transaction.builder()
                .id(idGenerator.randomUUID())
                .timestamp(timeProvider.now())
                .senderNumber(request.getSenderNumber())
                .receiverNumber(request.getReceiverNumber())
                .amount(request.getAmount())
                .status(NEW)
                .errorCause(SUCCESS);
    }

    private void audit(@Nonnull Transaction transaction) {
        var auditEvent = transactionMapper.toAuditEvent(transaction);
        auditService.saveAuditEvent(auditEvent);
    }

    // for testing purpose
    public void deleteAllAccounts() {
        accountLocks.clear();
        accountRepository.deleteAll();
    }

}
