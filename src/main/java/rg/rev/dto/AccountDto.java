package rg.rev.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    @JsonInclude(Include.NON_NULL)
    private UUID number;

    @Min(value = 0, message = "Balance cannot be negative")
    @Max(value = 10_000_000_000_000_000_00L, message = "Balance exceeded 10^16")
    private long balance;

    @NotEmpty(message = "Missing holder name")
    private String holderName;

}
