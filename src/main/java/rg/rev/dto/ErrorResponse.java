package rg.rev.dto;

import lombok.Value;

@Value
public class ErrorResponse {

    String errorMessage;
    String errorCode;
    
}
