package rg.rev.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Introspected
public class TransferDto {

    @JsonInclude(Include.NON_NULL)
    private UUID transactionId;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonInclude(Include.NON_NULL)
    private LocalDateTime timestamp;

    @NotNull(message = "Sender's number is missing")
    private UUID senderNumber;

    @NotNull(message = "Receiver's number is missing")
    private UUID receiverNumber;

    @Min(value = 1, message = "Amount must be positive")
    @Max(value = 1_000_000_000_000_00L, message = "Amount exceeded 10^12")
    private long amount;

}
