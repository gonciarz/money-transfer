package rg.rev.configs;

import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import rg.rev.model.Account;
import rg.rev.model.Transaction;

import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Factory
public class ApplicationConfig {

    @Bean
    @Named("accounts")
    public Map<UUID, Account> accounts() {
        return new HashMap<>();
    }

    @Bean
    @Named("accountLocks")
    public Map<UUID, ReentrantReadWriteLock> accountLocks() {
        return new ConcurrentHashMap<>();
    }

    @Bean
    @Named("transactions")
    public Queue<Transaction> transactions() {
        return new ConcurrentLinkedQueue<>();
    }

}
