package rg.rev.errors;

import org.junit.jupiter.api.Test;

import static org.assertj.vavr.api.VavrAssertions.assertThat;

class ErrorCauseExtractorTest {

    private final ErrorCauseExtractor extractor = new ErrorCauseExtractor();

    @Test
    void shouldHandleThrowable() {
        // given & when
        var errorCauseTry = extractor.extract(new Throwable("message"));

        // then
        assertThat(extractor.extract(new Throwable("message")))
                .isFailure();
    }

    @Test
    void shouldHandleAppException() {
        // given & when
        var errorCauseTry = extractor.extract(AppException.error(ErrorCause.SENDER_NOT_EXIST));

        // then
        assertThat(errorCauseTry)
                .isSuccess()
                .contains(ErrorCause.SENDER_NOT_EXIST);
    }

}