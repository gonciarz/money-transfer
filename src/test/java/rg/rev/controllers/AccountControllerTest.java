package rg.rev.controllers;

import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.test.annotation.MockBean;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;
import rg.rev.dto.AccountDto;
import rg.rev.model.Account;
import rg.rev.services.AccountService;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@MicronautTest
class AccountControllerTest {

    @Inject
    private EmbeddedServer server;

    @Inject
    @Client("/accounts")
    private HttpClient client;

    @Inject
    private AccountService accountService;

    @Test
    void shouldReturnNoAccountsWhenNoRecords() {

        when(accountService.allAccounts()).thenReturn(List.of());

        var response = client.toBlocking()
                .exchange(HttpRequest.GET("/"),
                        Argument.of(List.class, AccountDto.class));

        assertThat(response.code())
                .isEqualTo(200);

        assertThat(response.body())
                .hasSize(0);
    }

    @Test
    void shouldAddAccount() {

        when(accountService.createAccount(anyString(), anyLong())).then(args -> Try.success(
                new Account(
                        UUID.randomUUID(),
                        args.getArgument(1, Long.class),
                        args.getArgument(0, String.class)
                )
        ));

        var account = AccountDto.builder()
                .number(UUID.randomUUID())
                .holderName("John Snow")
                .balance(100_00L)
                .build();

        var createdAccountResponse = client.toBlocking()
                .exchange(HttpRequest.POST("/", account), AccountDto.class);

        assertThat(createdAccountResponse.code())
                .isEqualTo(HttpStatus.CREATED.getCode());

        assertThat(createdAccountResponse.body())
                .isEqualToIgnoringGivenFields(account, "number");
    }

    @MockBean(AccountService.class)
    AccountService accountService() {
        return mock(AccountService.class);
    }
}