package rg.rev.controllers;

import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.test.annotation.MockBean;
import org.junit.jupiter.api.Test;
import rg.rev.model.Transaction;
import rg.rev.services.AuditService;

import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static rg.rev.MockData.AUDIT_EVENT;

@MicronautTest
class AuditControllerTest {

    @Inject
    private EmbeddedServer server;

    @Inject
    @Client("/audits")
    private HttpClient client;

    @Inject
    private AuditService auditService;

    @Test
    void shouldReturnListOfTransactions() {

        when(auditService.allAuditEvents()).thenReturn(List.of(AUDIT_EVENT));

        var response = client.toBlocking()
                .exchange(HttpRequest.GET("/"),
                        Argument.of(List.class, Transaction.class));

        assertThat(response.code())
                .isEqualTo(200);

        assertThat(response.body())
                .hasSize(1);
    }

    @MockBean(AuditService.class)
    AuditService transactionService() {
        return mock(AuditService.class);
    }

}