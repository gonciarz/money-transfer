package rg.rev.controllers;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.test.annotation.MockBean;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;
import rg.rev.dto.TransferDto;
import rg.rev.errors.ErrorHandler;
import rg.rev.services.AccountService;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static rg.rev.MockData.NUMBER_ACCOUNT_A;
import static rg.rev.MockData.NUMBER_ACCOUNT_B;
import static rg.rev.TestUtils.createTransaction;

@MicronautTest
class TransferControllerTest {

    @Inject
    private EmbeddedServer server;

    @Inject
    @Client("/transfers")
    private HttpClient client;

    @Inject
    private AccountService accountService;

    @Inject
    private ErrorHandler errorHandler;

    @Test
    void shouldReturnBadRequestWhenImproperBody() {
        // given
        var transferRequest = TransferDto.builder()
                .senderNumber(NUMBER_ACCOUNT_A)
                .amount(0L)
                .build();

        // when & then
        try {
            client.toBlocking().exchange(HttpRequest.POST("/", transferRequest));
        } catch (HttpClientResponseException e) { // Micronaut's client doesn't handle Bad response nicely
            assertThat(e.getResponse().code())
                    .isEqualTo(400);
        }
    }

    @Test
    void shouldReturnSuccessWhenTransactionCreated() {
        // given
        var transferRequest = TransferDto.builder()
                .senderNumber(NUMBER_ACCOUNT_A)
                .receiverNumber(NUMBER_ACCOUNT_B)
                .amount(100_00L)
                .build();

        var transaction = createTransaction(transferRequest);
        when(accountService.transfer(eq(transferRequest))).thenReturn(Try.success(transaction));

        // when
        var response = client.toBlocking().exchange(HttpRequest.POST("/", transferRequest));

        // then
        verify(errorHandler, never()).createErrorResponse(any(Throwable.class));
        assertThat(response.code()).isEqualTo(200);
    }

    @MockBean(AccountService.class)
    AccountService accountService() {
        return mock(AccountService.class);
    }

    @MockBean(ErrorHandler.class)
    ErrorHandler errorHandler() {
        return mock(ErrorHandler.class);
    }
}