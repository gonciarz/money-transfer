package rg.rev;

import io.vavr.control.Try;
import rg.rev.dto.TransferDto;
import rg.rev.model.Account;
import rg.rev.model.Transaction;
import rg.rev.repository.AccountRepository;
import rg.rev.utils.IdGenerator;
import rg.rev.utils.TimeProvider;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class TestContext {

    private final AccountRepository accountRepository;
    private final Map<UUID, ReentrantReadWriteLock> accountLocks;
    private final IdGenerator idGenerator;
    private final TimeProvider timeProvider;

    public TestContext(AccountRepository accountRepository,
                       Map<UUID, ReentrantReadWriteLock> accountLocks,
                       IdGenerator idGenerator,
                       TimeProvider timeProvider) {
        this.accountRepository = accountRepository;
        this.accountLocks = accountLocks;
        this.idGenerator = idGenerator;
        this.timeProvider = timeProvider;
    }

    public Account createAccount(long balance) {
        return createAccount(balance, true);
    }

    public Account createAccount(long balance, boolean createStub) {
        var id = UUID.randomUUID();
        var account = Account.builder().id(id).balance(balance).build();
        if (createStub) {
            when(accountRepository.findById(eq(id))).thenReturn(Try.success(account));
            when(accountLocks.get(eq(id))).thenReturn(new ReentrantReadWriteLock(true));
        }
        return account;
    }

    public Transaction createTransaction(TransferDto transferRequest) {
        var id = UUID.randomUUID();
        var now = LocalDateTime.now();
        when(idGenerator.randomUUID()).thenReturn(id);
        when(timeProvider.now()).thenReturn(now);
        return TestUtils.createTransaction(transferRequest, id, now);
    }

}
