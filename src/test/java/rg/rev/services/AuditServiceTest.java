package rg.rev.services;

import io.vavr.control.Try;
import org.assertj.core.api.Assertions;
import org.assertj.vavr.api.VavrAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import rg.rev.model.AuditEvent;
import rg.rev.repository.AuditRepository;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuditServiceTest {

    private static final AuditEvent AUDIT_EVENT = AuditEvent.builder()
            .transactionId(UUID.randomUUID())
            .build();

    @Mock
    private AuditRepository auditRepository;

    private AuditService auditService;

    @BeforeEach
    void setUp() {
        auditService = new AuditService(auditRepository);
    }

    @Test
    void shouldSaveAuditEvent() {
        // given
        when(auditRepository.save(any(AuditEvent.class))).thenReturn(Try.success(AUDIT_EVENT));

        // when
        var savedEvent = auditService.saveAuditEvent(AUDIT_EVENT);

        // then
        VavrAssertions.assertThat(savedEvent)
                .isSuccess()
                .contains(AUDIT_EVENT);
    }

    @Test
    void shouldListAllAuditEvents() {
        // given
        when(auditRepository.findAll()).thenReturn(List.of(AUDIT_EVENT));

        // when
        var auditEvents = auditService.allAuditEvents();

        // then
        Assertions.assertThat(auditEvents)
                .containsOnly(AUDIT_EVENT);
    }

}