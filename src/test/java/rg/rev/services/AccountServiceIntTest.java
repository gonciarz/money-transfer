package rg.rev.services;

import io.micronaut.test.annotation.MicronautTest;
import io.vavr.control.Try;
import org.assertj.vavr.api.VavrAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import rg.rev.dto.TransferDto;
import rg.rev.model.Account;
import rg.rev.model.Transaction;

import javax.inject.Inject;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;
import static rg.rev.TestUtils.createRequests;
import static rg.rev.TestUtils.createTransaction;
import static rg.rev.TestUtils.createTransferRequest;

@MicronautTest
class AccountServiceIntTest {

    @Inject
    private AccountService accountService;

    @BeforeEach
    void setUp() {
        accountService.deleteAllAccounts();
    }

    @Test
    void shouldTransferMoneySingleRequest() {
        // given
        var accountA = accountService.createAccount("A", 100_00L);
        var accountB = accountService.createAccount("B", 0L);

        VavrAssertions.assertThat(accountA).isSuccess();
        VavrAssertions.assertThat(accountB).isSuccess();

        var transferRequest = createTransferRequest(accountA, accountB, 100_00L);

        // when
        var transactionResult = accountService.transfer(transferRequest);

        // then
        VavrAssertions.assertThat(transactionResult).isSuccess();

        var expectedTransaction = createTransaction(transferRequest);

        assertThat(transactionResult.get())
                .hasNoNullFieldsOrProperties()
                .isEqualToIgnoringGivenFields(expectedTransaction, "id", "timestamp");

        assertThat(retrieveAccountBalances())
                .containsEntry("A", 0L)
                .containsEntry("B", 100_00L);
    }

    @Test
    void shouldProcessChainOfTransfersSequentially() {

        // given
        var accountA = accountService.createAccount("A", 100_00L);
        var accountB = accountService.createAccount("B", 0L);
        var accountC = accountService.createAccount("C", 0L);

        var transferRequest1 = createTransferRequest(accountA, accountB, 100_00L);
        var transferRequest2 = createTransferRequest(accountB, accountC, 100_00L);

        // when
        var transaction1 = accountService.transfer(transferRequest1);
        var transaction2 = accountService.transfer(transferRequest2);

        // then
        VavrAssertions.assertThat(transaction1).isSuccess();
        VavrAssertions.assertThat(transaction2).isSuccess();

        assertThat(retrieveAccountBalances())
                .containsEntry("A", 0L)
                .containsEntry("B", 0L)
                .containsEntry("C", 100_00L);
    }

    @RepeatedTest(10)
    void shouldProcessChainOfTransfersInParallel() {

        // A(100) -100  -> B(0)   -100-> C(0)     // B should wait
        // A(100) -100  -> B(100) -100-> C(0)     // B doesn't need to wait
        
        // given
        var accountA = accountService.createAccount("A", 100_00L);
        var accountB = accountService.createAccount("B", 0L);
        var accountC = accountService.createAccount("C", 0L);
        var transferRequest1 = createTransferRequest(accountA, accountB, 100_00L);
        var transferRequest2 = createTransferRequest(accountB, accountC, 100_00L);

        var executor = Executors.newFixedThreadPool(2);

        // when
        var results = Stream.of(transferRequest1, transferRequest2)
                .map(request -> CompletableFuture.supplyAsync(() -> accountService.transfer(request), executor))
                .map(CompletableFuture::join)
                .collect(Collectors.toList());

        results.stream()
                .map(VavrAssertions::assertThat)
                .forEach(result -> result.isSuccess());

        assertThat(retrieveAccountBalances())
                .containsEntry("A", 0L)
                .containsEntry("B", 0L)
                .containsEntry("C", 100_00L);
    }

    @RepeatedTest(10)
    void shouldProcessManyTransfersWithoutStarving() {

        // given
        var initBalance = 1_000_000_000_000_00L;
        var accountA = accountService.createAccount("A", initBalance);
        var accountB = accountService.createAccount("B", initBalance);
        var accountC = accountService.createAccount("C", initBalance);

        var transferRequests = createRequests(accountA, accountB, accountC, 100, 50);

        var results = transferRequests.stream()
                .map(this::transferCommonPool)
                .map(CompletableFuture::join)
                .collect(Collectors.toList());

        var successfulTransactions = results.stream()
                .filter(Try::isSuccess)
                .count();

        assertThat(successfulTransactions)
                .isEqualTo(300L);

        assertThat(retrieveAccountBalances())
                .containsEntry("A", 50_50_00_00_49_50_00L)
                .containsEntry("B", 1_00_00_49_49_50_50_00L)
                .containsEntry("C", 1_49_49_50_50_00_00_00L);
    }

    @RepeatedTest(10)
    void shouldProcessCycleTransactions() {

        // given
        var initBalance = 100_00L;
        var accountA = accountService.createAccount("A", initBalance);
        var accountB = accountService.createAccount("B", initBalance);
        var transferRequest1 = createTransferRequest(accountA, accountB, 100_00L);
        var transferRequest2 = createTransferRequest(accountB, accountA, 100_00L);

        var executor = Executors.newFixedThreadPool(2);
        
        // when
        var results = Stream.of(transferRequest1, transferRequest2)
                .map(request -> CompletableFuture.supplyAsync(() -> accountService.transfer(request), executor))
                .map(CompletableFuture::join)
                .collect(Collectors.toList());

        results.stream()
                .map(VavrAssertions::assertThat)
                .forEach(result -> result.isSuccess());

        assertThat(retrieveAccountBalances())
                .containsEntry("A", 100_00L)
                .containsEntry("B", 100_00L);
    }

    @RepeatedTest(10)
    void shouldNotTransferWhenSenderHasNoLongerEnoughMoney() {

        // A(100) -100-> B(0)
        // A(100*) -100-> C(0) // this should be not possible

        // given
        var accountA = accountService.createAccount("A", 100_00L);
        var accountB = accountService.createAccount("B", 0L);
        var accountC = accountService.createAccount("C", 0L);

        var transferRequest1 = createTransferRequest(accountA, accountB, 100_00L);
        var transferRequest2 = createTransferRequest(accountA, accountC, 100_00L);

        var executor = Executors.newFixedThreadPool(2);

        // when
        var results = Stream.of(transferRequest1, transferRequest2)
                .map(request -> CompletableFuture.supplyAsync(() -> accountService.transfer(request), executor))
                .map(CompletableFuture::join)
                .collect(Collectors.toList());

        // then
        var validTransactions = results.stream()
                .filter(Try::isSuccess)
                .count();

        assertThat(validTransactions)
                .isEqualTo(1);

        assertThat(retrieveAccountBalances())
                .containsEntry("A", 0L)
                .containsAnyOf(
                        entry("B", 100_00L),
                        entry("C", 100_00L));
    }

    private CompletableFuture<Try<Transaction>> transferCommonPool(TransferDto request) {
        return CompletableFuture.supplyAsync(() -> accountService.transfer(request), ForkJoinPool.commonPool());
    }

    private Map<String, Long> retrieveAccountBalances() {
        return accountService.allAccounts().stream()
                .collect(Collectors.toMap(Account::getHolderName, Account::getBalance));
    }

}