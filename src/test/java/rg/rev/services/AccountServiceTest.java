package rg.rev.services;

import io.vavr.control.Try;
import org.assertj.vavr.api.VavrAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import rg.rev.TestContext;
import rg.rev.errors.AppException;
import rg.rev.errors.ErrorCauseExtractor;
import rg.rev.mappers.TransactionMapper;
import rg.rev.model.Account;
import rg.rev.model.AuditEvent;
import rg.rev.repository.AccountRepository;
import rg.rev.utils.IdGenerator;
import rg.rev.utils.TimeProvider;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static rg.rev.MockData.ACCOUNT_A;
import static rg.rev.TestUtils.createTransferRequest;
import static rg.rev.errors.ErrorCause.RECEIVER_NOT_EXIST;
import static rg.rev.errors.ErrorCause.SAME_ACCOUNTS;
import static rg.rev.errors.ErrorCause.SENDER_INSUFFICIENT_BALANCE;
import static rg.rev.errors.ErrorCause.SENDER_NOT_EXIST;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @Mock
    private AuditService auditService;

    @Mock
    private Map<UUID, ReentrantReadWriteLock> accountLocks;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private IdGenerator idGenerator;

    @Mock
    private TimeProvider timeProvider;

    private AccountService accountService;

    private TestContext context;

    @BeforeEach
    void setup() {
        var transactionMapper = Mappers.getMapper(TransactionMapper.class);
        accountService = new AccountService(accountRepository, auditService, transactionMapper,
                new ErrorCauseExtractor(), idGenerator, timeProvider, accountLocks);
        context = new TestContext(accountRepository, accountLocks, idGenerator, timeProvider);
    }

    @Test
    void shouldCreateAccount() {
        // given
        when(accountRepository.save(any(Account.class))).thenReturn(Try.success(ACCOUNT_A));

        // when
        accountService.createAccount(ACCOUNT_A.getHolderName(), 0);

        // then
        verify(accountRepository, times(1)).save(any(Account.class));
        verify(auditService, never()).saveAuditEvent(any(AuditEvent.class));
    }

    @Test
    void shouldRemoveAllAccounts() {
        // when
        accountService.deleteAllAccounts();

        // then
        verify(accountLocks, times(1)).clear();
        verify(accountRepository, times(1)).deleteAll();
    }

    @Test
    void shouldNotTransferMoneyWhenInsufficientBalance() {
        // given
        var accountA = context.createAccount(100_00L);
        var accountB = context.createAccount(10_00L);
        var transferRequest = createTransferRequest(accountA, accountB, 200_00);
        var transaction = context.createTransaction(transferRequest);

        // when
        var result = accountService.transfer(transferRequest);

        // then
        VavrAssertions.assertThat(result)
                .isFailure()
                .failBecauseOf(AppException.class)
                .failReasonHasMessage(SENDER_INSUFFICIENT_BALANCE.getMessage());
    }

    @Test
    void shouldNotTransferMoneyToSameAccount() {
        // given
        var accountA = context.createAccount(300_00L, false);
        var transferRequest = createTransferRequest(accountA, accountA, 200_00);
        var transaction = context.createTransaction(transferRequest);

        // when
        var result = accountService.transfer(transferRequest);

        // then
        VavrAssertions.assertThat(result)
                .isFailure()
                .failBecauseOf(AppException.class)
                .failReasonHasMessage(SAME_ACCOUNTS.getMessage());
    }

    @Test
    void shouldNotTransferMoneyWhenSenderDoesNotExist() {
        // given
        var accountA = context.createAccount(300_00L, false);
        var accountB = context.createAccount(10_00L, false);

        when(accountRepository.findById(eq(accountA.getId()))).thenReturn(Try.of(() -> null));
        when(accountRepository.findById(eq(accountB.getId()))).thenReturn(Try.success(accountB));

        var transferRequest = createTransferRequest(accountA, accountB, 200_00);
        var transaction = context.createTransaction(transferRequest);

        // when
        var result = accountService.transfer(transferRequest);

        // then
        VavrAssertions.assertThat(result)
                .isFailure()
                .failBecauseOf(AppException.class)
                .failReasonHasMessage(SENDER_NOT_EXIST.getMessage());
    }

    @Test
    void shouldNotTransferMoneyWhenReceiverDoesNotExist() {
        // given
        var accountA = context.createAccount(300_00L, false);
        var accountB = context.createAccount(10_00L, false);

        when(accountRepository.findById(eq(accountA.getId()))).thenReturn(Try.success(accountA));
        when(accountRepository.findById(eq(accountB.getId()))).thenReturn(Try.of(() -> null));

        var transferRequest = createTransferRequest(accountA, accountB, 200_00);
        var transaction = context.createTransaction(transferRequest);

        // when
        var result = accountService.transfer(transferRequest);

        // then
        VavrAssertions.assertThat(result)
                .isFailure()
                .failBecauseOf(AppException.class)
                .failReasonHasMessage(RECEIVER_NOT_EXIST.getMessage());
    }

    @Test
    void shouldNotTransferMoneyWhenInternalError() {

        // given
        var accountA = context.createAccount(200_00L, false);
        var accountB = context.createAccount(50_00L, false);
        when(accountRepository.findById(eq(accountA.getId()))).thenThrow(new RuntimeException("expected"));

        var transferRequest = createTransferRequest(accountA, accountB, 75);
        var transaction = context.createTransaction(transferRequest);

        // when
        var result = accountService.transfer(transferRequest);

        // then
        VavrAssertions.assertThat(result)
                .isFailure()
                .failBecauseOf(RuntimeException.class)
                .failReasonHasMessage("expected");
    }

    @Test
    void shouldTransferMoney() {

        // given
        var accountA = context.createAccount(200_00L);
        var accountB = context.createAccount(50_00L);
        var transferRequest = createTransferRequest(accountA, accountB, 75);
        var transaction = context.createTransaction(transferRequest);

        // when
        var result = accountService.transfer(transferRequest);

        // then
        VavrAssertions.assertThat(result)
                .isSuccess()
                .contains(transaction);
    }

}