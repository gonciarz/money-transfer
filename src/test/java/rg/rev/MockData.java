package rg.rev;

import rg.rev.errors.ErrorCause;
import rg.rev.model.Account;
import rg.rev.model.AuditEvent;
import rg.rev.model.TransferState;

import java.time.LocalDateTime;
import java.util.UUID;

public class MockData {

    private MockData() {
    }

    public static final UUID NUMBER_ACCOUNT_A = UUID.randomUUID();
    public static final UUID NUMBER_ACCOUNT_B = UUID.randomUUID();

    public static final Account ACCOUNT_A = Account.builder()
            .id(NUMBER_ACCOUNT_A)
            .holderName("A")
            .balance(1000_00L)
            .build();
    
    public static final AuditEvent AUDIT_EVENT = AuditEvent.builder()
            .transactionId(UUID.randomUUID())
            .timestamp(LocalDateTime.now())
            .senderNumber(NUMBER_ACCOUNT_A)
            .receiverNumber(NUMBER_ACCOUNT_B)
            .transferAmount(100_00L)
            .transactionState(TransferState.COMPLETED)
            .errorCause(ErrorCause.SUCCESS)
            .build();
}
