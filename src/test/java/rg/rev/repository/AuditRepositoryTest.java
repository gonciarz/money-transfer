package rg.rev.repository;

import io.vavr.control.Try;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ConcurrentLinkedQueue;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static rg.rev.MockData.AUDIT_EVENT;

class AuditRepositoryTest {

    private AuditRepository auditRepository;

    @BeforeEach
    void setUp() {
        auditRepository = new AuditRepository(new ConcurrentLinkedQueue<>());
    }

    @Test
    void shouldSaveAndFind() {
        // when
        var savedRecord = auditRepository.save(AUDIT_EVENT);
        var records = auditRepository.findAll();

        // then
        assertThat(savedRecord)
                .isEqualTo(Try.success(AUDIT_EVENT));

        assertThat(records)
                .containsExactly(AUDIT_EVENT);
    }

    @Test
    void shouldDeleteAll() {
        // when & then
        assertThatThrownBy(() -> auditRepository.deleteAll())
                .isInstanceOf(UnsupportedOperationException.class);
    }
}