package rg.rev.repository;

import io.vavr.control.Try;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rg.rev.model.Account;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static rg.rev.MockData.ACCOUNT_A;

class AccountRepositoryTest {

    private AccountRepository accountRepository;

    private Map<UUID, Account> accounts;

    @BeforeEach
    void setUp() {
        accounts = new HashMap<>();
        accountRepository = new AccountRepository(accounts);
    }

    @Test
    void shouldSaveAndFind() {
        // when
        var savedRecord = accountRepository.save(ACCOUNT_A);
        var records = accountRepository.findAll();

        // then
        assertThat(savedRecord)
                .isEqualTo(Try.success(ACCOUNT_A));

        assertThat(records)
                .containsExactly(ACCOUNT_A);
    }

    @Test
    void shouldDeleteAll() {
        // when
        accountRepository.deleteAll();

        // then
        assertThat(accounts.isEmpty())
                .isTrue();
    }

}