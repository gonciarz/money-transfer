package rg.rev;

import io.vavr.control.Try;
import rg.rev.dto.TransferDto;
import rg.rev.errors.ErrorCause;
import rg.rev.model.Account;
import rg.rev.model.Transaction;
import rg.rev.model.TransferState;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Map.entry;

public class TestUtils {

    private TestUtils() {
    }

    public static TransferDto createTransferRequest(Account sender, Account receiver, long amount) {
        return TransferDto.builder()
                .senderNumber(sender.getId())
                .receiverNumber(receiver.getId())
                .amount(amount)
                .build();
    }

    public static TransferDto createTransferRequest(Try<Account> sender, Try<Account> receiver, long amount) {
        return createTransferRequest(sender.get(), receiver.get(), amount);
    }

    public static Transaction createTransaction(TransferDto transferRequest) {
        var id = UUID.randomUUID();
        var now = LocalDateTime.now();
        return createTransaction(transferRequest, id, now);
    }

    public static Transaction createTransaction(TransferDto transferRequest, UUID id, LocalDateTime timestamp) {
        return Transaction.builder()
                .id(id)
                .timestamp(timestamp)
                .senderNumber(transferRequest.getSenderNumber())
                .receiverNumber(transferRequest.getReceiverNumber())
                .amount(transferRequest.getAmount())
                .status(TransferState.COMPLETED)
                .errorCause(ErrorCause.SUCCESS)
                .build();
    }

    public static List<TransferDto> createRequests(Try<Account> accountA,
                                                   Try<Account> accountB,
                                                   Try<Account> accountC,
                                                   int factor, int copies) {
        var counter = new AtomicLong(1);
        var requests = permutationStream(accountA, accountB, accountC)
                .map(e -> createTransferRequest(e.getKey(), e.getValue(), counter.updateAndGet(f -> f * factor)))
                .flatMap(e -> Collections.nCopies(copies, e).stream())
                .collect(Collectors.toList());
        Collections.shuffle(requests);
        return requests;
    }

    public static <T> Stream<Map.Entry<T, T>> permutationStream(T accountA, T accountB, T accountC) {
        return Stream.of(
                entry(accountA, accountB),
                entry(accountB, accountC),
                entry(accountC, accountA))
                .flatMap(e -> Stream.of(e, reverseEntry(e)));
    }

    public static <T> Map.Entry<T, T> reverseEntry(Map.Entry<T, T> e) {
        return entry(e.getValue(), e.getKey());
    }


}
