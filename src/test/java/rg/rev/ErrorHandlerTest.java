package rg.rev;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import rg.rev.dto.ErrorResponse;
import rg.rev.errors.ErrorCause;
import rg.rev.errors.ErrorCauseExtractor;
import rg.rev.errors.ErrorHandler;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.of;
import static rg.rev.errors.AppException.error;
import static rg.rev.errors.ErrorCause.INTERNAL_ERROR;
import static rg.rev.errors.ErrorCause.SENDER_NOT_EXIST;

@SuppressWarnings("unchecked")
class ErrorHandlerTest {

    private final ErrorHandler errorHandler = new ErrorHandler(new ErrorCauseExtractor());

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("inputs")
    void shouldHandle(String displayName, Throwable throwable,
                      ErrorCause errorCause, String message) {

        var response = errorHandler.createErrorResponse(throwable);

        assertThat(response.getBody())
                .isPresent()
                .get()
                .isInstanceOf(ErrorResponse.class)
                .isEqualTo(new ErrorResponse(errorCause.getErrorCode(), message));

        assertThat(response.getStatus().getCode())
                .isEqualTo(errorCause.getHttpStatus().getCode());
    }

    private static Stream<Arguments> inputs() {
        return Stream.of(
                of("Throwable with no message", new Throwable(), INTERNAL_ERROR, INTERNAL_ERROR.getMessage()),
                of("Throwable with message", new Throwable("message"), INTERNAL_ERROR, "message"),
                of("AppException", error(SENDER_NOT_EXIST), SENDER_NOT_EXIST, SENDER_NOT_EXIST.getMessage())
        );
    }

}